package demo

import (
	"fmt"
	"os"

	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/rest/authz/client"
	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/rest/common"
	"github.com/fnproject/fn/api/server"
	"github.com/fnproject/fn/fnext"
)

type sphinxDemo struct {
	ARSClient client.ARSClient
}

func (d *sphinxDemo) Name() string {
	return "bitbucket.org/bill828/fn-sphinx-demo"
}

func (d *sphinxDemo) Setup(s fnext.ExtServer) error {
	d.initSphinxARSClient()
	listener := sphinxListener{
		ARSClient: d.ARSClient,
	}
	s.AddAppListener(&listener)
	s.AddCallListener(&listener)
	s.AddAPIMiddleware(&appMiddleware{})
	s.AddRootMiddleware(&rootMiddleware{
		ARSClient: d.ARSClient,
	})
	return nil
}

func (d *sphinxDemo) initSphinxARSClient() {
	clientID := os.Getenv("SPHINX_CLIENT_ID")
	clientSecret := os.Getenv("SPHINX_CLIENT_SECRET")
	tenantID := os.Getenv("SPHINX_TENANT_ID")

	fmt.Printf("Client ID: %s, Client Secret: %s, Tenant ID: %s.\n", clientID, clientSecret, tenantID)

	connectionProperties := map[string]string{
		common.CLIENT_ID_PROP:     clientID,
		common.CLIENT_SECRET_PROP: clientSecret,
		common.TENANT_ID_PROP:     tenantID,
	}

	client, err := client.New(connectionProperties)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error in creating a new client due to error %v.\n", err)
		os.Exit(1)
	}
	d.ARSClient = client
}

func init() {
	server.RegisterExtension(&sphinxDemo{})
}
