package demo

import (
	"context"
	"fmt"

	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/rest/authz/client"
	"github.com/fnproject/fn/api/models"
)

type sphinxListener struct {
	ARSClient client.ARSClient
}

// BeforeAppCreate called right before creating App in the database
func (d *sphinxListener) BeforeAppCreate(ctx context.Context, app *models.App) error {
	fmt.Printf("#### Listener.BeforeAppCreate: %v\n", app)
	return nil
}

// AfterAppCreate called after creating App in the database
func (d *sphinxListener) AfterAppCreate(ctx context.Context, app *models.App) error {
	fmt.Printf("#### Listener.AfterAppCreate: %v\n", app)
	return nil
}

// BeforeAppUpdate called right before updating App in the database
func (d *sphinxListener) BeforeAppUpdate(ctx context.Context, app *models.App) error {
	fmt.Printf("#### Listener.BeforeAppUpdate: %v\n", app)
	return nil
}

// AfterAppUpdate called after updating App in the database
func (d *sphinxListener) AfterAppUpdate(ctx context.Context, app *models.App) error {
	fmt.Printf("#### Listener.AfterAppUpdate: %v\n", app)
	return nil
}

// BeforeAppDelete called right before deleting App in the database
func (d *sphinxListener) BeforeAppDelete(ctx context.Context, app *models.App) error {
	fmt.Printf("#### Listener.BeforeAppDelete: %s\n", app.Name)
	return nil
}

// AfterAppDelete called after deleting App in the database
func (d *sphinxListener) AfterAppDelete(ctx context.Context, app *models.App) error {
	fmt.Printf("#### Listener.AfterAppDelete: %s\n", app.Name)
	return nil
}

// BeforeAppGet called right before getting an app
func (d *sphinxListener) BeforeAppGet(ctx context.Context, appName string) error {
	fmt.Printf("#### Listener.BeforeAppGet: %s\n", appName)
	return nil
}

// AfterAppGet called after getting app from database
func (d *sphinxListener) AfterAppGet(ctx context.Context, app *models.App) error {
	fmt.Printf("#### Listener.AfterAppGet: %s\n", app.Name)
	return nil
}

// BeforeAppsList called right before getting a list of all user's apps. Modify the filter to adjust what gets returned.
func (d *sphinxListener) BeforeAppsList(ctx context.Context, filter *models.AppFilter) error {
	fmt.Printf("#### Listener.BeforeAppsList: %s\n", filter.Name)
	return nil
}

// AfterAppsList called after deleting getting a list of user's apps. apps is the result after applying AppFilter.
func (d *sphinxListener) AfterAppsList(ctx context.Context, apps []*models.App) error {
	fmt.Printf("#### Listener.AfterAppsList: %v\n", apps)
	return nil
}

// BeforeCall called before a function is executed
func (d *sphinxListener) BeforeCall(ctx context.Context, call *models.Call) error {
	fmt.Printf("#### Listener.BeforeCall: %s\n", call.Path)
	return nil
}

// AfterCall called after a function completes
func (d *sphinxListener) AfterCall(ctx context.Context, call *models.Call) error {
	fmt.Printf("#### Listener.AfterCall: %s\n", call.Path)
	return nil
}
