package mgmt

type PMSClient interface {
	CreateService(service *Service) error
	GetService(serviceName string) (*Service, error)
	DeleteService(serviceName string) error
	CreatePolicy(serviceName string, policy *Policy) error
	CreateRolePolicy(serviceName string, rolePolicy *RolePolicy) error
}
