package mgmt

type Permission struct {
	Resource           string   `json:"resource,omitempty"`
	ResourceExpression string   `json:"resourceExpression,omitempty"`
	Actions            []string `json:"actions,omitempty"`
}

type Policy struct {
	ID          string        `json:"id"`
	Name        string        `json:"name"`
	Effect      string        `json:"effect,omitempty"`
	Permissions []*Permission `json:"permissions,omitempty"`
	Principals  [][]string    `json:"principals,omitempty"`
}

const (
	Grant = "grant"
	Deny  = "deny"
)

type RolePolicy struct {
	ID                  string     `json:"id"`
	Name                string     `json:"name"`
	Effect              string     `json:"effect,omitempty"`
	Roles               []string   `json:"roles,omitempty"`
	Principals          [][]string `json:"principals,omitempty"`
	Resources           []string   `json:"resources,omitempty"`
	ResourceExpressions []string   `json:"resourceExpressions,omitempty"`
}

type Service struct {
	Name         string        `json:"name" binding:"required"`
	Type         string        `json:"type,omitempty"`
	Policies     []*Policy     `json:"policies,omitempty"`
	RolePolicies []*RolePolicy `json:"rolePolicies,omitempty"`
}
