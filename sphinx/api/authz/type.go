package authz

type Principal struct {
	Type string `json:"type,omitempty"`
	Name string `json:"name,omitempty"`
	IDD  string `json:"idd,omitempty"`
}

func (p *Principal) String() string {
	return "{" + "\"type\": \"" + p.Type + "\", \"name\": \"" + p.Name + "\", \"idd\":\"" + p.IDD + "\"}"
}

type Subject struct {
	Principals []*Principal           `json:"principals,omitempty"`
	Attributes map[string]interface{} `json:"attributes,omitempty"`
	TokenType  string                 `json:"tokenType,omitempty"`
	Token      string                 `json:"token,omitempty"`
}

type RequestContext struct {
	Subject     *Subject               `json:"subject,omitempty"`
	ServiceName string                 `json:"serviceName,omitempty"`
	Resource    string                 `json:"resource,omitempty"`
	Action      string                 `json:"action,omitempty"`
	Attributes  map[string]interface{} `json:"attributes,omitempty"`
}
