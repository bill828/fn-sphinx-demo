package common

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

var propertyDefalutValues = map[string]string{
	HOST_PROP:      "a.authz.fun",
	PMS_PORT_PROP:  "6733",
	ARS_PORT_PROP:  "6734",
	TMS_PORT_PROP:  "6735",
	IS_SECURE_PROP: "true",
}

// NewHTTPClient returns a HTTP client instance for coming endpoint
func NewHTTPClient(endpoint string) (*http.Client, error) {
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	proxyURL, err := http.ProxyFromEnvironment(req)
	if err != nil {
		return nil, err
	}

	httpTrans := &http.Transport{}
	if proxyURL != nil {
		log.Printf("Using proxy %s", proxyURL)
		httpTrans.Proxy = http.ProxyURL(proxyURL)
	}

	return &http.Client{
		Timeout:   10 * time.Second,
		Transport: httpTrans,
	}, nil
}

// GetPropertyValue retrieves a property value with default value.
func GetPropertyValue(properties map[string]string, propName string) (string, error) {
	value, ok := properties[propName]
	if !ok {
		value, ok = propertyDefalutValues[propName]
		if !ok {
			return "", fmt.Errorf("Property %s is not defined", propName)
		}
	}
	return value, nil
}

// DecodeJSONPayload decode JSON payload from HTTP request
func DecodeJSONResponse(resp *http.Response, outObj interface{}) error {
	decoder := json.NewDecoder(resp.Body)
	return decoder.Decode(outObj)
}

const (
	HOST_PROP              = "host"
	PMS_PORT_PROP          = "pms.port"
	ARS_PORT_PROP          = "ars.port"
	TMS_PORT_PROP          = "tms.port"
	IS_SECURE_PROP         = "is.secure"
	CLIENT_ID_PROP         = "client.id"
	CLIENT_SECRET_PROP     = "client.secret"
	TENANT_MGMT_TOKEN_PROP = "tenant.mgmt.token"
	TENANT_ID_PROP         = "tenant.id"
)
