package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/api/authz"
	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/rest/common"
)

type restARSClient struct {
	isAllowedEndpoint string
	tokenReqEndpoint  string
	clientID          string
	clientSecret      string
	httpClient        *http.Client
	currentToken      *tokenContent
	timer             time.Timer
}

// New returns a new ARSClient instance
func New(properties map[string]string) (ARSClient, error) {
	host, _ := common.GetPropertyValue(properties, common.HOST_PROP)
	arsPort, _ := common.GetPropertyValue(properties, common.ARS_PORT_PROP)
	tmsPort, _ := common.GetPropertyValue(properties, common.TMS_PORT_PROP)
	isSecureStr, _ := common.GetPropertyValue(properties, common.IS_SECURE_PROP)
	clientID, err := common.GetPropertyValue(properties, common.CLIENT_ID_PROP)
	if err != nil {
		return nil, err
	}
	clientSecret, err := common.GetPropertyValue(properties, common.CLIENT_SECRET_PROP)
	if err != nil {
		return nil, err
	}
	tenantID, err := common.GetPropertyValue(properties, common.TENANT_ID_PROP)
	if err != nil {
		return nil, err
	}

	protocol := "https"
	isSecure, err := strconv.ParseBool(isSecureStr)
	if err != nil {
		return nil, err
	}
	if !isSecure {
		protocol = "http"
	}

	isAllowedEndpoint := fmt.Sprintf("%s://%s:%s/%s/authz-check/v1/is-allowed", protocol, host, arsPort, tenantID)
	tokenReqEndpoint := fmt.Sprintf("%s://%s:%s/%s/tenant-mgmt/v1/token", protocol, host, tmsPort, tenantID)

	log.Printf("Endpoint of is-allowed API: %s\n", isAllowedEndpoint)
	log.Printf("Endpoint of token request API: %s\n", tokenReqEndpoint)
	log.Printf("Client ID is %s\n", clientID)

	httpClient, err := common.NewHTTPClient(isAllowedEndpoint)
	if err != nil {
		return nil, err
	}

	return &restARSClient{
		isAllowedEndpoint: isAllowedEndpoint,
		tokenReqEndpoint:  tokenReqEndpoint,
		httpClient:        httpClient,
		clientID:          clientID,
		clientSecret:      clientSecret,
	}, nil
}

func convertToJSONAttribute(attrs map[string]interface{}) []*jsonAttribute {
	ret := []*jsonAttribute{}
	if attrs == nil {
		return ret
	}
	for key, value := range attrs {
		ret = append(ret, &jsonAttribute{
			Name:  key,
			Value: value,
		})
	}
	return ret
}

func convertToJSONRequest(context *authz.RequestContext) *jsonContext {
	var subject *jsonSubject
	if context.Subject != nil {
		subject = &jsonSubject{
			Principals: context.Subject.Principals,
			TokenType:  context.Subject.TokenType,
			Token:      context.Subject.Token,
			Attributes: convertToJSONAttribute(context.Subject.Attributes),
		}
	}
	return &jsonContext{
		Subject:     subject,
		ServiceName: context.ServiceName,
		Resource:    context.Resource,
		Action:      context.Action,
		Attributes:  convertToJSONAttribute(context.Attributes),
	}
}

func (c *restARSClient) refreshToken() {
	if c.currentToken == nil {
		return
	}

	if c.currentToken.Expired > time.Now().UnixNano() {
		// If current token is expired.
		if err := c.requestToken(); err != nil {
			log.Printf("Unable to request token due to error %v.\n", err)
		}
	}
}

func (c *restARSClient) requestToken() error {
	log.Printf("Requesting a new token with client ID: %s.\n", c.clientID)
	req, err := http.NewRequest(http.MethodPost, c.tokenReqEndpoint, bytes.NewBuffer([]byte("grant_type=client_credentials")))
	req.SetBasicAuth(c.clientID, c.clientSecret)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Unexpected status %d returned", resp.StatusCode)
	}

	decoder := json.NewDecoder(resp.Body)
	tokenContent := tokenContent{}
	if err := decoder.Decode(&tokenContent); err != nil {
		return err
	}

	// Expired token before 1 minute
	after := time.Duration(tokenContent.ExpiresIn-60) * time.Second
	tokenContent.Expired = int64(after) + time.Now().UnixNano()
	c.currentToken = &tokenContent

	time.AfterFunc(after, c.refreshToken)
	return nil
}

func (c *restARSClient) IsAllowed(context authz.RequestContext) (bool, error) {
	if c.currentToken == nil {
		if err := c.requestToken(); err != nil {
			return false, err
		}
	}
	payload, err := json.Marshal(convertToJSONRequest(&context))
	if err != nil {
		return false, err
	}

	log.Printf("Token for ARS is %s.\n", c.currentToken.Token)
	req, err := http.NewRequest(http.MethodPost, c.isAllowedEndpoint, bytes.NewBuffer(payload))
	if err != nil {
		return false, err
	}
	req.Header.Add("Context-Type", "application/json")
	req.Header.Add("Authorization", c.currentToken.TokenType+" "+c.currentToken.Token)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return false, fmt.Errorf("Status %d returned", resp.StatusCode)
	}

	var result jsonIsAllowedResponse
	reader := json.NewDecoder(resp.Body)
	if err := reader.Decode(&result); err != nil {
		return false, err
	}
	return result.Allowed, nil
}
