package client

import (
	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/api/authz"
)

type tokenContent struct {
	Token     string `json:"access_token"`
	TokenType string `json:"token_type"`
	ExpiresIn int    `json:"expires_in"`
	Expired   int64
}

type jsonAttribute struct {
	Name  string      `json:"name"`
	Type  string      `json:"type"`
	Value interface{} `json:"value"`
}

type jsonSubject struct {
	Principals []*authz.Principal `json:"principals"`
	Attributes []*jsonAttribute   `json:"attributes"`
	TokenType  string             `json:"tokenType"`
	Token      string             `json:"token"`
}

type jsonContext struct {
	Subject     *jsonSubject     `json:"subject"`
	ServiceName string           `json:"serviceName"`
	Resource    string           `json:"resource"`
	Action      string           `json:"action"`
	Attributes  []*jsonAttribute `json:"attributes"`
}

type jsonIsAllowedResponse struct {
	Allowed      bool   `json:"allowed"`
	ErrorMessage string `json:"errorMessage,omitempty"`
}
