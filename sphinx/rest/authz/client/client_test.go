package client

import (
	"log"
	"testing"

	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/api/authz"
)

var connectionProperties = map[string]string{
	authz.CLIENT_ID_PROP:     "client-CxsGja",
	authz.CLIENT_SECRET_PROP: "ReTMyGdRZa",
	authz.TENANT_ID_PROP:     "f605c878-8f15-48fd-8f00-1d269e6018ee",
}

func TestRequestToken(t *testing.T) {
	// client, err := New("a.authz.fun", "f605c878-8f15-48fd-8f00-1d269e6018ee", false, "client-CxsGja", "ReTMyGdRZa", "/home/jiefu/a.auth.fun/a_authz_fun.crt")
	client, err := New(connectionProperties)
	if err != nil {
		log.Printf("Error in creating a new client due to error %v.\n", err)
		t.FailNow()
	}
	impl, _ := client.(*restARSClient)
	if err := impl.requestToken(); err != nil {
		log.Printf("Error in requesting a new token due to error %v.\n", err)
		t.FailNow()
	}

	if impl.currentToken == nil {
		log.Printf("Current token should not be null.\n")
		t.FailNow()
	}

	log.Printf("Token details: %v.\n", impl.currentToken)
}

func TestIsAllowed(t *testing.T) {
	client, err := New(connectionProperties)
	if err != nil {
		log.Printf("Error in creating a new client due to error %v.\n", err)
		t.FailNow()
	}

	context := authz.RequestContext{
		Subject: &authz.Subject{
			User: "alan.cao",
		},
		ServiceName: "acao",
		Resource:    "/home/acao/Downloads",
		Action:      "read",
	}
	allowed, err := client.IsAllowed(context)
	if err != nil {
		log.Printf("Error in calling IsAllowed due to error %v.\n", err)
		t.FailNow()
	}
	log.Printf("Evaluation result: %v.", allowed)
	if !allowed {
		log.Printf("Wrong result %v.", allowed)
	}
}
