package main

import (
	"log"
	"os"

	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/api/authz"
	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/rest/authz/client"
	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/rest/common"
)

func main() {
	connectionProperties := map[string]string{
		common.CLIENT_ID_PROP:     "client-CxsGja",
		common.CLIENT_SECRET_PROP: "ReTMyGdRZa",
		common.TENANT_ID_PROP:     "baj1aebm3ls000aef490",
	}

	client, err := client.New(connectionProperties)
	if err != nil {
		log.Printf("Error in creating a new client due to error %v.\n", err)
		os.Exit(1)
	}

	context := authz.RequestContext{
		Subject: &authz.Subject{
			Principals: []*authz.Principal{
				&authz.Principal{
					Type: "user",
					Name: "alan.cao",
				},
			},
		},
		ServiceName: "acao",
		Resource:    "/home/acao/Downloads",
		Action:      "read",
	}
	allowed, err := client.IsAllowed(context)
	if err != nil {
		log.Printf("Error in calling IsAllowed due to error %v.\n", err)
		os.Exit(1)
	}
	log.Printf("Evaluation result: %v.", allowed)
}
