package client

import "bitbucket.org/bill828/fn-sphinx-demo/sphinx/api/authz"

// ARSClient is a client interface for ARS service
type ARSClient interface {
	IsAllowed(authz.RequestContext) (bool, error)
}
