package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/api/mgmt"
	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/rest/common"
)

type restPMSClient struct {
	policyMgmtPrefix string
	policyMgmtToken  string
	httpClient       *http.Client
}

// New returns a new PMS client instance
func New(properties map[string]string) (mgmt.PMSClient, error) {
	host, _ := common.GetPropertyValue(properties, common.HOST_PROP)
	pmsPort, _ := common.GetPropertyValue(properties, common.PMS_PORT_PROP)
	isSecureStr, _ := common.GetPropertyValue(properties, common.IS_SECURE_PROP)
	tenantID, err := common.GetPropertyValue(properties, common.TENANT_ID_PROP)
	if err != nil {
		return nil, err
	}
	token, err := common.GetPropertyValue(properties, common.TENANT_MGMT_TOKEN_PROP)
	if err != nil {
		return nil, err
	}

	protocol := "https"
	isSecure, err := strconv.ParseBool(isSecureStr)
	if err != nil {
		return nil, err
	}
	if !isSecure {
		protocol = "http"
	}

	policyMgmtPrefix := fmt.Sprintf("%s://%s:%s/%s/policy-mgmt/v1/", protocol, host, pmsPort, tenantID)

	httpClient, err := common.NewHTTPClient(policyMgmtPrefix)
	if err != nil {
		return nil, err
	}

	log.Printf("Prefix of PMS endpoint is: %s\n", policyMgmtPrefix)
	log.Printf("Policy Management Token is: %s\n", token)

	return &restPMSClient{
		policyMgmtPrefix: policyMgmtPrefix,
		policyMgmtToken:  token,
		httpClient:       httpClient,
	}, nil
}

func (c *restPMSClient) CreateService(service *mgmt.Service) error {
	payload, err := json.Marshal(service)
	if err != nil {
		return err
	}

	endpoint := c.policyMgmtPrefix + "service"
	req, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(payload))
	if err != nil {
		return err
	}
	req.Header.Add("Context-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+c.policyMgmtToken)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Status %d returned", resp.StatusCode)
	}

	return nil
}

func (c *restPMSClient) GetService(serviceName string) (*mgmt.Service, error) {
	endpoint := c.policyMgmtPrefix + "service/" + serviceName
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Context-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+c.policyMgmtToken)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Status %d returned", resp.StatusCode)
	}

	var ret mgmt.Service
	if err := common.DecodeJSONResponse(resp, &ret); err != nil {
		return nil, err
	}

	return &ret, nil
}

func (c *restPMSClient) DeleteService(serviceName string) error {
	endpoint := c.policyMgmtPrefix + "service/" + serviceName
	req, err := http.NewRequest(http.MethodDelete, endpoint, nil)
	if err != nil {
		return err
	}
	req.Header.Add("Context-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+c.policyMgmtToken)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("Status %d returned", resp.StatusCode)
	}

	return nil
}

func (c *restPMSClient) CreatePolicy(serviceName string, policy *mgmt.Policy) error {
	payload, err := json.Marshal(policy)
	if err != nil {
		return err
	}

	endpoint := fmt.Sprintf("%s/service/%s/policy", c.policyMgmtPrefix, serviceName)
	req, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(payload))
	if err != nil {
		return err
	}
	req.Header.Add("Context-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+c.policyMgmtToken)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Status %d returned", resp.StatusCode)
	}

	return nil
}

func (c *restPMSClient) CreateRolePolicy(serviceName string, rolePolicy *mgmt.RolePolicy) error {
	payload, err := json.Marshal(rolePolicy)
	if err != nil {
		return err
	}

	endpoint := fmt.Sprintf("%s/service/%s/role-policy", c.policyMgmtPrefix, serviceName)
	req, err := http.NewRequest(http.MethodPost, endpoint, bytes.NewBuffer(payload))
	if err != nil {
		return err
	}
	req.Header.Add("Context-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+c.policyMgmtToken)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Status %d returned", resp.StatusCode)
	}

	return nil
}
