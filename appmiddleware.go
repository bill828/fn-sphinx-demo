package demo

import (
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/api/authz"

	"bitbucket.org/bill828/fn-sphinx-demo/sphinx/rest/authz/client"
)

type appMiddleware struct {
}

func (m *appMiddleware) Handle(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("#### appMiddleware.Handle(): url: %v.\n", r.URL)
		next.ServeHTTP(w, r)
	})
}

type rootMiddleware struct {
	ARSClient client.ARSClient
}

func (m *rootMiddleware) Handle(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// fmt.Printf("#### rootMiddleware.Handle(): url: %v, method %s, headers %v.\n", r.URL, r.Method, r.Header)
		// var requestContext *authz.RequestContext
		// if strings.HasPrefix(r.URL.Path, "/v1/") {
		// 	requestContext = m.initSphinxRequestContextForApp(r)
		// } else if strings.HasPrefix(r.URL.Path, "/r/") {
		// 	requestContext = m.initSphinxRequestContextForCall(r)
		// } else {
		// 	fmt.Printf("**** Unknown URL.\n")
		// }
		// if requestContext != nil {
		// 	// has requestContext
		// 	allowed, err := m.ARSClient.IsAllowed(*requestContext)
		// 	fmt.Printf("#### Sphinx call context %v, result: %v, error: %v.\n", requestContext, allowed, err)
		// 	if err != nil {
		// 		fmt.Fprintf(os.Stderr, "**** Sphinx error: %v.\n", err)
		// 	}
		// 	if !allowed {
		// 		w.WriteHeader(http.StatusForbidden)
		// 		return
		// 	}
		// }
		next.ServeHTTP(w, r)
	})
}

func (m *rootMiddleware) initRequestSubject(r *http.Request) *authz.Subject {
	atzHeader := r.Header.Get("Authorization")
	if strings.HasPrefix(atzHeader, "Bearer ") {
		// Bearer token
		return &authz.Subject{
			TokenType: "WERCKER",
			Token:     atzHeader[7:],
		}
	}
	return nil
}

func (m *rootMiddleware) initRequestAttributes(r *http.Request) map[string]interface{} {
	ret := make(map[string]interface{})
	for key, value := range r.Header {
		ret[key] = value
	}
	return ret
}

func (m *rootMiddleware) initSphinxRequestContextForApp(r *http.Request) *authz.RequestContext {
	resource := r.URL.Path[3:]
	action := strings.ToLower(r.Method)
	service := "fnapis"

	return &authz.RequestContext{
		Subject:     m.initRequestSubject(r),
		ServiceName: service,
		Resource:    resource,
		Action:      action,
		Attributes:  m.initRequestAttributes(r),
	}
}

func (m *rootMiddleware) initSphinxRequestContextForCall(r *http.Request) *authz.RequestContext {
	resource := r.URL.Path[2:]
	action := strings.ToLower(r.Method)
	service := "apps"

	return &authz.RequestContext{
		Subject:     m.initRequestSubject(r),
		ServiceName: service,
		Resource:    resource,
		Action:      action,
		Attributes:  m.initRequestAttributes(r),
	}
}
